import 'dart:io';
import 'dart:math';

int parseInput() => int.parse(stdin.readLineSync() ?? "0");
int getRandomNumber(int max) => Random().nextInt(max) + 1;

void printMainMenu() => print("""
    Pick a difficulty:
    1. Easy
    2. Medium
    3. Hard
    4. Exit
    your choice: """);

int getDifficulty(int choice) {
  switch (choice) {
    case 1:
      return 20;

    case 2:
      return 15;

    case 3:
      return 10;

    case 4:
      exit(0);

    default:
      print("Invalid choice!!");
      return 0;
  }
}

main() {
  var maxTries = 0;
  var randNumb = getRandomNumber(100);

  while (maxTries == 0) {
    printMainMenu();
    maxTries = getDifficulty(parseInput());
  }

  for (var i = 0; i < maxTries; i++) {
    stdout.write("Guess the number");
    var guess = int.parse(stdin.readLineSync() ?? "0");

    if (guess == randNumb) {
      print("You guessed it right!!");
      exit(0);
    } else if (guess > randNumb) {
      print("Too high!!");
    } else {
      print("Too low!!");
    }
  }
}
