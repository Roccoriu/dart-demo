# Dart Tutorial

This is a quick introduction and comparison of Dart and Java. It's not meant to be a comprehensive tutorial, but rather a quick overview of the syntax and features of Dart. If you're already familiar with Java, this should be enough to get you started with Dart.

To do the coding exercises you can do the following online compiler and sdk:
[Try Dart](https://dart.dev/#try-dart)

## Classes

---

Here's some examples of classes in Java and Dart. The syntax is very similar. The main difference is that in Dart, the constructor is defined in the class body, whereas in Java, the constructor is defined as a method with the same name as the class. Also, in Dart, the constructor can be defined as a one-liner, as shown below. Compared to the Java code there is much less boilerplate code in Dart.

### Java

---

```java
public class Person {
    private int age;
    private String name;
    // constructor
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void showOutput() {
        System.out.println(name);
        System.out.println(age);
    }
}
```

<br>

### Dart

---

```dart
class Person {
    int age;
    String name;
    //constructor
    Person(this.name, this.age);

    void showOutput() {
        print(name);
        print(age);
    }
}
```

<br>

## Interfaces

---

Interfaces are defined in a similar way in both Java and Dart. The main difference is that in Dart, the interface is defined as an abstract class. In Java, the interface is defined as an interface. In both languages, the interface is implemented by a class using the `implements` keyword.

### Java

---

```java
public interface Person {
    public void showOutput();
}
//implement person interface
public class Student implements Person {
    private String name;
    private int age;
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public void showOutput() {
        System.out.println(name);
        System.out.println(age);
    }
}
```

### Dart

---

```dart
abstract class Person {
  void showOutput();
}

//implement person interface
class Student implements Person {
  String name;
  int age;
  Student(this.name, this.age);
  void showOutput() {
    print(name);
    print(age);
  }
}
```

## Generics

---

Generics are defined in a similar way in both Java and Dart. The main difference is that in Dart, the generic type is defined in angle brackets, whereas in Java, the generic type is defined in angle brackets. In both languages, the generic type is used in the class definition.

### Java

---

```java
public class Box<T> {
    private T content;

    public Box(T content) {
        this.content = content;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}

public class Main {
    public static void main(String[] args) {
        Box<String> box = new Box<String>("Hello World");
        System.out.println(box.getContent());
    }
}
```

### Dart

---

```dart
class Box<T> {
    T content;
    Box(this.content);

    T getContent() {
        return content;
    }

    void setContent(T content) {
        this.content = content;
    }
}

void main() {
    Box<String> box = Box<String>("Hello World");
    print(box.getContent());
}
```

## Collections

---

Collections are defined in a similar way in both Java and Dart. The main difference is that in Dart, the generic type is defined in angle brackets, whereas in Java, the generic type is defined in angle brackets. In both languages, the generic type is used in the class definition.

### Java

---

```java
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

class main {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("Hello");
        list.add("World");
        System.out.println(list);

        HashSet<String> set = new HashSet<String>();
        set.add("Hello");
        set.add("World");
        System.out.println(set);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Hello", "World");
        System.out.println(map);
    }
}
```

### Dart

---

```dart
void main() {
    List<String> list = ["Hello", "World"];
    print(list);

    Set<String> set = {"Hello", "World"};
    print(set);

    Map<String, String> map = {"Hello": "World"};
    print(map);
}
```

<br>

## Enums

---

Enums are defined in a similar way in both Java and Dart. The main difference is that in Dart, the enum is defined as a class, whereas in Java, the enum is defined as an enum. In both languages, the enum is used in the class definition.

### Java

---

```java
public enum Color {
    RED, GREEN, BLUE
}

public class Main {
    public static void main(String[] args) {
        Color color = Color.RED;
        System.out.println(color);
    }
}
```

### Dart

---

```dart
// enum in dart
enum Color {
  red,
  green,
  blue,
  yellow,
}
```
